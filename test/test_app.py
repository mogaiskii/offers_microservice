import os
import unittest
import json
from dotenv import load_dotenv

from app import app
from connector import Connector
from models import User, Offer
from auth_util import get_token


class CreateTestCase(unittest.TestCase):
    def setUp(self):
        load_dotenv()
        db_url = os.getenv('TEST_DB_URL')
        Connector().set_connection(db_url)
        session = Connector().session

        self.user = User('test', 'test@test.com', 'password123')
        session.add(self.user)
        session.commit()

        # only after commit user has id!
        self.token = get_token(self.user)

    def test_alive(self):
        try:
            request, response = app.test_client.post(
                '/offers/create',
                headers={
                    'authorization': self.token
                },
                data=json.dumps({
                  'title': 'title',
                  'text': 'text'
                })
            )
            self.assertEqual(response.status, 201)
        except:
            self.assertTrue(False)

    def test_offer_returns_correctly(self):
        try:
            request, response = app.test_client.post(
                '/offers/create',
                headers={
                    'authorization': self.token
                },
                data=json.dumps({
                  'title': 'title',
                  'text': 'text'
                })
            )

            response_body = json.loads(response.body)
            real = response_body
            self.assertEqual(real['title'], 'title')
            self.assertEqual(real['text'], 'text')
            self.assertEqual(real['user_id'], self.user.id)
        except:
            self.assertTrue(False)

    def tearDown(self):
        session = Connector().session
        session.query(Offer).delete()
        session.query(User).delete()
        session.commit()


class ListTestCase(unittest.TestCase):
    def setUp(self):
        load_dotenv()
        db_url = os.getenv('TEST_DB_URL')
        Connector().set_connection(db_url)
        self.session = Connector().session

        self.user = User('test', 'test@test.com', 'password123')
        self.session.add(self.user)
        self.session.commit()

        # only after commit user has id!
        self.token = get_token(self.user)

    def test_alive(self):
        try:
            request, response = app.test_client.post(
                '/offers',
                headers={
                    'authorization': self.token
                }
            )
            self.assertEqual(response.status, 200)
        except:
            self.assertTrue(False)

    def test_offer_returns_correctly(self):
        try:
            offer = Offer(self.user, 'title', 'text')
            self.session.add(offer)
            self.session.commit()

            request, response = app.test_client.post(
                '/offers',
                headers={
                    'authorization': self.token
                }
            )

            response_body = json.loads(response.body)
            real = response_body
            self.assertEqual(len(real), 1)
            self.assertEqual(real[0]['id'], offer.id)
            self.assertEqual(real[0]['title'], offer.title)
            self.assertEqual(real[0]['text'], offer.text)
            self.assertEqual(real[0]['user_id'], offer.user_id)
        except:
            self.assertTrue(False)

    def tearDown(self):
        session = Connector().session
        session.query(Offer).delete()
        session.query(User).delete()
        session.commit()


class GetTestCase(unittest.TestCase):
    def setUp(self):
        load_dotenv()
        db_url = os.getenv('TEST_DB_URL')
        Connector().set_connection(db_url)
        self.session = Connector().session

        self.user = User('test', 'test@test.com', 'password123')
        self.session.add(self.user)
        self.session.commit()
        self.offer = Offer(self.user, 'title', 'text')

        # TODO: unite
        self.session.add(self.offer)
        self.session.commit()

        # only after commit user has id!
        self.token = get_token(self.user)

    def test_alive(self):
        try:
            request, response = app.test_client.post(
                '/offers/%s' % self.offer.id,
                headers={
                    'authorization': self.token
                }
            )
            self.assertEqual(response.status, 200)
        except:
            self.assertTrue(False)

    def test_offer_returns_correctly(self):
        try:
            request, response = app.test_client.post(
                '/offers/%s' % self.offer.id,
                headers={
                    'authorization': self.token
                }
            )

            response_body = json.loads(response.body)
            real = response_body
            self.assertEqual(real['id'], self.offer.id)
            self.assertEqual(real['title'], self.offer.title)
            self.assertEqual(real['text'], self.offer.text)
            self.assertEqual(real['user_id'], self.offer.user_id)
        except:
            self.assertTrue(False)

    def tearDown(self):
        session = Connector().session
        session.query(Offer).delete()
        session.query(User).delete()
        session.commit()
