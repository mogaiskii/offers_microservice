import os
import unittest
import dotenv

from connector import Connector


class ConnectorSmokeTestCase(unittest.TestCase):
    def setUp(self):
        dotenv.load_dotenv()
        self.db_url = os.getenv('DB_URL')

    def test_can_connect(self):
        has_exceptions = False
        try:
            Connector().set_connection(self.db_url)
        except:
            has_exceptions = True
        self.assertFalse(has_exceptions)

    def tearDown(self):
        Connector().session.close()


class ConnectorTestCase(unittest.TestCase):
    def setUp(self):
        dotenv.load_dotenv()
        self.db_url = os.getenv('DB_URL')

    def test_same_all_time(self):
        Connector().set_connection(self.db_url)
        c1 = Connector().session
        c2 = Connector().session
        self.assertEqual(c1, c2)
