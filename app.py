import os
import json
from sanic import Sanic
from sanic.response import json as json_response, raw as raw_response
from dotenv import load_dotenv
from sqlalchemy import or_
from connector import Connector
from models import User, Offer
from auth_util import get_token, authenticate

app = Sanic()


@app.route('/offers/create', methods=['POST'])
async def registry(request):
    try:
        request_user = authenticate(request)
        if not request_user:
            return raw_response(b'', status=401)

        request_data = json.loads(request.body)
        title = request_data['title']
        text = request_data['text']

        session = Connector().session
        offer = Offer(request_user, title, text)
        session.add(offer)
        session.commit()

        return json_response(
            {
                'id': offer.id,
                'user_id': offer.user_id,
                'title': offer.title,
                'text': offer.text
            },
            status=201
        )
    except (json.decoder.JSONDecodeError, AttributeError):
        return raw_response(b'', status=400)


@app.route('/offers', methods=['POST'])
async def registry(request):
    request_user = authenticate(request)
    if not request_user:
        return raw_response(b'', status=401)

    session = Connector().session
    offers = session.query(Offer).filter_by(
        user_id=request_user.id
        ).all()

    return json_response(
        [
            {
                'id': offer.id,
                'user_id': offer.user_id,
                'title': offer.title,
                'text': offer.text
            } for offer in offers if offer
        ],
        status=200
    )


@app.route('/offers/<offer_id:int>', methods=['POST'])
async def registry(request, offer_id):
    request_user = authenticate(request)
    if not request_user:
        return raw_response(b'', status=401)

    session = Connector().session
    offer = session.query(Offer).get(offer_id)
    if not offer:
        return raw_response(b'', status=404)

    return json_response(
        {
            'id': offer.id,
            'user_id': offer.user_id,
            'title': offer.title,
            'text': offer.text
        },
        status=200
    )


if __name__ == '__main__':
    load_dotenv()
    db_url = os.getenv('DB_URL')
    Connector().set_connection(db_url)
    port = os.getenv('APP_PORT', 8000)
    app.run(host="0.0.0.0", port=port)
